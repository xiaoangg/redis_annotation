#include <stdio.h>
#include <fcntl.h>

#define REGISTER_API(name) \
    moduleRegisterApi("RedisModule_" #name, (void *)(unsigned long)RM_ ## name)

void getRandomBytes(unsigned char *p, size_t len) {

    printf("getRandomBytes 指针（地址）的值为：OX%p\n",p); 
    p += 1;
    printf("getRandomBytes 指针（地址）的值为：OX%p\n",p); 
}

void getRandomHexChars(char *p, size_t len) {
    printf("getRandomHexChars 指针（地址）的值为：OX%p\n",p); 
    getRandomBytes((unsigned char*)p,len);
}

int moduleRegisterApi(const char *funcname, void *funcptr) {
    printf("%s",funcname); 
    return 1;
}
void *RM_Alloc(size_t bytes) {
}


int getSeed()
{
    static int seed_initialized = 0;
    static int randNum; /* The SHA1 seed, from /dev/urandom. */
    static int counter = 0; /* The counter we hash with the seed. */

    if (seed_initialized)
    {
        printf("randNum is %d\n", randNum);
        return 1;
    }
    
    FILE *fd = fopen("/dev/urandom","r");
    if(fd ==NULL)
    {
        printf("error\n");
        return 1;
    }

    if (fread(&randNum,sizeof(char),1,fd) != 1){
        printf("error\n");
        return 1;
    }
    fclose(fd);
    printf("randNum is %d\n", randNum);
    seed_initialized =1;
}
/**
 * 利用Linux的/dev/urandom文件产生较好的随机数
 * */ 
int main()
{
    int randNum = 0;
  
    getSeed();
    getSeed();

    REGISTER_API(Alloc);
    char hashseed[16];

    printf("hashseed指针（地址）的值为：OX%p\n",hashseed);
    getRandomHexChars(hashseed, 1);
    printf("hashseed指针（地址）的值为：OX%p\n",hashseed);
  
    return 0;
}

