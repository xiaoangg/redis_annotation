#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
 
#define MAX_FD_NUM 1000
 

//设置套接字为非堵塞式
void setnonblock(int fd) {
    //fcntl 文件控制 F_GETFL:获取套接字属主
    int flag = fcntl(fd, F_GETFL, 0);
    if (flag == -1) {
        printf("get fcntl flag %s\n", strerror(errno));
        return;
    }
    //设置套接字为非堵塞式I/O型
    int ret = fcntl(fd, F_SETFL, flag | O_NONBLOCK);
    if (ret == -1) {
        printf("set fcntl non-blocking %s\n", strerror(errno));
        return;
    }
}
 
int socket_create(int port) {
    //AF_INET ipv4协议  SOCK_STREAM：字节流套接字
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("socket create %s\n", strerror(errno));
        return -1;
    }else{
        printf("socket create success fd %d\n", fd);
    }


    //设置套接字为非堵塞式I/O
    setnonblock(fd);
    struct sockaddr_in addr;
    //将某一块内存中的内容全部设置为指定的值。通常为新申请的内存做初始化工作
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    //把本地协议地址赋值给一个套接字
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        printf("socket bind %s\n", strerror(errno));
        return -1;
    }
    // 参数2 套接字排队的最大连接数
    if (listen(fd, 20) == -1) {
        printf("socket listen %s\n", strerror(errno));
        return -1;
    }else{
        printf("socket listen success\n");
    }
    return fd;
}
 
void socket_accept(int fd) {
    //创建epoll句柄; MAX_FD_NUM用来告诉内核这个监听的数目一共有多大
    int epfd = epoll_create(MAX_FD_NUM);
    if (epfd == -1) {
        printf("epoll create %s\n", strerror(errno));
        return;
    }
    struct epoll_event event, events[MAX_FD_NUM];
    memset(&event, 0, sizeof(event));
    event.data.fd = fd;
    event.events = EPOLLIN | EPOLLERR | EPOLLHUP;

    //注册要监听的事件类型
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &event) == -1) {
        printf("epoll ctl %s\n", strerror(errno));
        return;
    }
    int client_fd;
    while (1) {
        
        // epoll_wait 返回值：0表示超时；-1表示错误；大于0表示返回的需要处理的事件数目
        int num = epoll_wait(epfd, events, MAX_FD_NUM, -1);
        
        if (num == -1) {
            printf("epoll wait %s\n", strerror(errno));
            break;
        } else {
            int i = 0;
            for (; i<num; ++i) {
                if (events[i].data.fd == fd) {
                    struct sockaddr_in client_addr;
                    memset(&client_addr, 0, sizeof(client_addr));
                    int len = sizeof(client_addr);
                    client_fd = accept(fd, (struct sockaddr *)&client_addr, &len);
                    if (client_fd == -1) {
                        printf("socket accept %s\n", strerror(errno));
                        return;
                    }
                    setnonblock(client_fd);
                    event.data.fd = client_fd;
                    event.events = EPOLLIN | EPOLLERR | EPOLLHUP;
                    if (epoll_ctl(epfd, EPOLL_CTL_ADD, client_fd, &event) == -1) {
                        printf("epoll ctl %s\n", strerror(errno));
                        return;
                    }
                    continue;
                } else if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP)) { //出错或者挂起事件
                    printf("epoll err\n");
                    close(events[i].data.fd);
                    continue;
                } else {
                    char buf[64];
                    memset(buf, 0, sizeof(buf));
                    recv(events[i].data.fd, buf, sizeof(buf), 0);
                    printf("%s", buf);
                    close(events[i].data.fd);
                    continue;
                }
            }
        }
    }
}
 
void server(int port) {
    int fd = socket_create(port);
    if (fd == -1) {
        printf("socket create fd failed\n");
        return;
    }
    socket_accept(fd);
}
 
int main(int argc, char *argv[]) {
    int port = atoi(argv[1]);
    server(port);
    return 0;
}
