#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
 
int socket_connect(const char *ip, int port) {
    char * ip2 = "127.0.0.1";
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("socket create %s\n", strerror(errno));
        return -1;
    }
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip2);
    if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        printf("socket connect %s\n", strerror(errno));
        return -1;
    }
    return fd;
}
 
void socket_send(int fd) {
    char buf[64];
    char buf2[64];
    
    memset(buf, 0, sizeof(buf));
   // read(STDIN_FILENO, buf, sizeof(buf));
   // printf("client socket create success fd:%s\n", buf);
    //int res = send(fd, buf, strlen(buf), 0);

    memset(buf2, 0, sizeof(buf2));
    //buf2[0]='c';
    //buf2[1]='b';
    strcpy(buf2, "123\n");
    printf("client socket create success fd:%s\n", buf2);
    int retn = send(fd, buf2, strlen(buf2), 0);
    printf("retn :%d \n", retn);
    close(fd);
}
 
void client(const char *ip, int port) {
    int fd = socket_connect(ip, port);
    printf("client socket create success fd:%d\n", fd);
    if (fd == -1) {
        printf("client socket create failed\n");
        return;
    }
    socket_send(fd);
}
 
int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("give the error parameters\n");
        return 0;
    }
    const char *ip = argv[1];
    int port = atoi(argv[2]);
    for (size_t i = 0; i < 10000; i++)
    {
        client(ip, port);
    }
    
    
    return 0;
}
