# Top level makefile, the real shit is at src/Makefile

default: all
#Make命令提供一系列内置变量，$(MAKE) 指向当前使用的Make工具
#Make命令还提供一些自动变量，$@指代当前目标，就是Make命令当前构建的那个目标
#.DEFAULT表示找不到匹配规则时，就执行该命令
.DEFAULT:
	cd src && $(MAKE) $@

install:
	cd src && $(MAKE) $@

#声明“伪命令”
.PHONY: install