/*
 * Copyright (c) 2009-2012, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 * Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __INTSET_H
#define __INTSET_H
#include <stdint.h>

//整数集合的定义
//当一个集合只包含整数值元素；并且元素不多时，redis会使用整数集合作为集合键的底层实现；
typedef struct intset {
    uint32_t encoding; //编码方式 ，决定了contents中元素是int_16 int_32 int_64等
    uint32_t length;   //数组的长度
    int8_t contents[]; //顺序按照从小到大保存集合元素
} intset;

intset *intsetNew(void); //创建一个新的整数集合
intset *intsetAdd(intset *is, int64_t value, uint8_t *success);//将新的元素添加到整数结合中
intset *intsetRemove(intset *is, int64_t value, int *success);//移除某个元素
uint8_t intsetFind(intset *is, int64_t value); //查找某个元素是否在集合中
int64_t intsetRandom(intset *is); //整数集合中随机返回一个元素
uint8_t intsetGet(intset *is, uint32_t pos, int64_t *value); //取出给定索引的元素
uint32_t intsetLen(const intset *is); //返回元素个数
size_t intsetBlobLen(intset *is); //返回整数集合占中内存字节数量

#ifdef REDIS_TEST
int intsetTest(int argc, char *argv[]);
#endif

#endif // __INTSET_H
