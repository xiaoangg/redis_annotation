/* A simple event-driven programming library. Originally I wrote this code
 * for the Jim's event-loop (Jim is a Tcl interpreter) but later translated
 * it in form of a library for easy reuse.
 *
 * Copyright (c) 2006-2012, Salvatore Sanfilippo <antirez at gmail dot com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of Redis nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __AE_H__
#define __AE_H__

#include <time.h>

#define AE_OK 0
#define AE_ERR -1

#define AE_NONE 0       /* No events registered. */
#define AE_READABLE 1   /* Fire when descriptor is readable. */
#define AE_WRITABLE 2   /* Fire when descriptor is writable. */
#define AE_BARRIER 4    /* With WRITABLE, never fire the event if the
                           READABLE event already fired in the same event
                           loop iteration. Useful when you want to persist
                           things to disk before sending replies, and want
                           to do that in a group fashion. */

#define AE_FILE_EVENTS 1  /*文件事件 二进制 0b 01*/
#define AE_TIME_EVENTS 2  /*时间事件 二进制 0b 10*/
#define AE_ALL_EVENTS (AE_FILE_EVENTS|AE_TIME_EVENTS)
#define AE_DONT_WAIT 4
#define AE_CALL_AFTER_SLEEP 8

#define AE_NOMORE -1  //「时间事件」处理函数返回定义，如果是AE_NOMORE 表示是定时事件，事件执行一次后就会被删除；非AE_NOMORE表示是周期性事件；
#define AE_DELETED_EVENT_ID -1  // 标记已经删除的事件

/* Macros */
#define AE_NOTUSED(V) ((void) V)

struct aeEventLoop;

/**
 * typedef int aeTimeProc(struct aeEventLoop *eventLoop, long long id, void *clientData);
 * 上面的语句作用是 作一个函数声明类型.
 * 如：aeTimeProc func1 等价声明一个函数 func1(struct aeEventLoop *eventLoop, long long id, void *clientData);
 */ 
/* Types and data structures */
typedef void aeFileProc(struct aeventLoop *eventLp, int fd, void *clientData, int mask);
typedef int aeTimeProc(struct aeEventLoop *eventLoop, long long id, void *clientData);
typedef void aeEventFinalizerProc(struct aeEventLoop *eventLoop, void *clientData);
typedef void aeBeforeSleepProc(struct aeEventLoop *eventLoop);

/* File event structure */
typedef struct aeFileEvent {
    int mask; /* one of AE_(READABLE|WRITABLE|BARRIER) */
    aeFileProc *rfileProc; //可读处理函数
    aeFileProc *wfileProc; //可写处理函数
    void *clientData;
} aeFileEvent;

/**
 * 时间事件结构 
*/
/* Time event structure */
typedef struct aeTimeEvent {
    long long id; /* time event identifier. */ //事件id，全局唯一id，自增；

    //when_sec和when_ms 记录什么时候执行该事件；阅读 ae.c中processTimeEvents函数可以理解该两个属性的用途
    long when_sec; /* seconds */  // 记录事件到达的时间（秒）
    long when_ms; /* milliseconds */ //记录事件到达的时间（毫秒部分）（

    aeTimeProc *timeProc;
    aeEventFinalizerProc *finalizerProc; //最终执行的事件处理函数（ae.c中processTimeEvents函数中，可以看到该函数的调用逻辑）
    void *clientData;
    struct aeTimeEvent *prev;
    struct aeTimeEvent *next; //事件双向连表
} aeTimeEvent;

/* A fired event */
typedef struct aeFiredEvent {
    int fd;
    int mask;
} aeFiredEvent;

/**
 * 「事件」的基础信息 
*/
/* State of an event based program */
typedef struct aeEventLoop {
    int maxfd;   /* ighest file descriptor currently registered */ //当前注册的最大文件描述符id
    int setsize; /* max number of file descriptors tracked *///能监听的最大为文件描述符数量
    long long timeEventNextId;
    time_t lastTime;     /* Used to detect system clock skew */ //用于校验系统时钟偏差（ae.c中processTimeEvents函数）
    aeFileEvent *events; /* Registered events */ //注册的「文件事件」
    aeFiredEvent *fired; /* Fired events */ //触发的「文件事件」
    aeTimeEvent *timeEventHead; //时间事件连表的头指针
    int stop;
    void *apidata; /* This is used for polling API specific data */
    aeBeforeSleepProc *beforesleep;
    aeBeforeSleepProc *aftersleep;
} aeEventLoop;

/* Prototypes */
aeEventLoop *aeCreateEventLoop(int setsize);
void aeDeleteEventLoop(aeEventLoop *eventLoop);
void aeStop(aeEventLoop *eventLoop);
int aeCreateFileEvent(aeEventLoop *eventLoop, int fd, int mask,
        aeFileProc *proc, void *clientData);
void aeDeleteFileEvent(aeEventLoop *eventLoop, int fd, int mask);
int aeGetFileEvents(aeEventLoop *eventLoop, int fd);

long long aeCreateTimeEvent(aeEventLoop *eventLoop, long long milliseconds,
        aeTimeProc *proc, void *clientData,
        aeEventFinalizerProc *finalizerProc); //添加一个时间事件到服务器
int aeDeleteTimeEvent(aeEventLoop *eventLoop, long long id);
int aeProcessEvents(aeEventLoop *eventLoop, int flags); //事件调度函数
int aeWait(int fd, int mask, long long milliseconds);
void aeMain(aeEventLoop *eventLoop);
char *aeGetApiName(void);
void aeSetBeforeSleepProc(aeEventLoop *eventLoop, aeBeforeSleepProc *beforesleep);
void aeSetAfterSleepProc(aeEventLoop *eventLoop, aeBeforeSleepProc *aftersleep);
int aeGetSetSize(aeEventLoop *eventLoop);
int aeResizeSetSize(aeEventLoop *eventLoop, int setsize);

#endif
