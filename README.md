﻿﻿Redis 5.0.3 源码注释
===================================
本项目是注释版的 Redis 5.0.3 源码，
原始代码来自： https://github.com/antirez/redis 。
参考：http://redisbook.com/  https://github.com/huangz1990/redis-3.0-annotated

###附录：各个源码文件的作用简介

第一部分 基础数据结构与对象
=========================================
* **sds.c sds.h 简单动态字符串**  
    
    redis实现的简单动态字符串。
    
    sds相对于c字符串的优点
    * 常数复杂度获取字符串长度
    * 避免缓冲区溢出
    * 减少字符修改带来的内存重分配
    * 二进制安全

    **我的疑问**
    
    * sdsnewlen函数  unsigned char *fp;//指向结构体 flags的指针
    为什么不直接使用结构体->flags的方式来赋值


* **adlist.h、adlist.c**
    
    链表（双向）的实现

* **dict.c dict.h**

    字典的实现
    
* **t_zset.c**
    
    * 跳表基础数据结构实现（zls开头）
    * 有序集合数据类型的实现

* **intset.c intset.h**
    
    整数集合的实现


* **t_list.c**
    
    队列实现   

* **quicklist.c**
    
    快速链表实现

* **siphash.c**

    sip散列函数的实现

* **跳表**

    跳表数据结构定义位于server.h中
    接口实现在t_zset.c中


* **压缩列表**

    ziplist.h  
    ziplish.c

   **我的疑问**
    
    * 压缩列表的末尾节点使用255标记结束，如果列表中存在255节点是如何处理的

* **字符串**

    t_string.c

* **endianconv.c endianconv.h**

    字节序转换


* **object.c** 
    
    对象的创建和解析；  
    内存回收；
    

第二部分 数据库的实现
=========================================   

* **db.c db.h**
    
    数据库的相关实现

* **expire.c**

    键值生存时间相关实现；
    exipre、pexipre、expireat、pexipreat、ttl、persist等命令；

    过期键的回收

* **server.c**
    
    负责服务器的启动、维护和关闭等事项。

* **rdb.c rdb.h**
    
    RDB持久化服务实现    

* **roi.c**

    redis实现的一个简单的面向流的I/O抽象

* **aof.c**

    AOF持久化功能实现


* **bio.c  bio.h**

    BACKGROUND I/O

* **ae.c  ae.h ae_epoll.c ae_select.c ae_evport.c ae_kqueue.c**
    
    「文件事件」/「时间事件」驱动程序

* **anet.c anet.h**
    
    TCP socket的封装

* **networking.c**
    
    TCP socket的封装


* **sentinel.c**
    
    哨兵的实现


* **config.c**
    
    配置相关实现

第三部分 多机数据库的实现
=========================================   


* **replication.c**
    
    复制功能的实现，主从


* **blocked.c**
    
    堵塞相关
    
    例如断开所有堵塞的客户端

* **cluster.c cluster.h**
    
    集群的实现


第四部分 独立功能的实现
===========================================

* **pubsub.c**
    
    发布/订阅功能的实现


* **multi.c**
    
    事务的实现



* **sort.c qpsort.h qpsort.c**
    
    排序的实现 
    * qpsort.h qpsort.c 
        
        pqsort.c的快排流程是改编自一个经典实现，该实现被许多库的实现所使用。
        
        //TODO
        - [ ] 阅读该实现 对比c库函数qsort();

        基于NetBSD libc qsort实现，做了部分修改，以支持 范围排序；
 
* **bitop.c**

    位数组的实现

    [汉明重量百度百科](https://baike.baidu.com/item/%E6%B1%89%E6%98%8E%E9%87%8D%E9%87%8F/7110799#1)

    [知乎](https://zhuanlan.zhihu.com/p/165968167)


* **slowglog.c**

    慢查询日志的实现

其他
=============
* **util.c**
    小工具
        
* **atomicvar.h**

    实现原子计数
        
* **intset.h intset.c**
    
    整数集合实现；当一个集合只包含整数值元素；并且元素不多时，redis会使用整数集合作为集合键的底层实现；

* **redisassert.h**    

    redis自建的断言系统

* **evict.c**    

    Maxmemory指令处理（LRU逐出和其他策略）。数据淘汰策略


* **rax.c**    
    [基数树维基百科](https://zh.wikipedia.org/wiki/%E5%9F%BA%E6%95%B0%E6%A0%91)
    
    基数树的实现 A radix tree implementation
    
    **基础概念：**

    对于长整型数据的映射，如何解决Hash冲突和Hash表大小的设计是一个很头疼的问题。
    
    radix树就是针对这种稀疏的长整型数据查找，能快速且节省空间地完成映射。借助于Radix树，我们可以实现对于长整型数据类型的路由。
    
    利用radix树可以根据一个长整型（比如一个长ID）快速查找到其对应的对象指针。这比用hash映射来的简单，也更节省空间，使用hash映射hash函数难以设计，不恰当的hash函数可能增大冲突，或浪费空间。
    
    radix tree是一种多叉搜索树，树的叶子结点是实际的数据条目。每个结点有一个固定的指针指向子结点（每个指针称为槽slot，n为划分的基的大小）

* **notify.c**

    TODO
    notify.c干啥用的
疑问
====================================
* 配置项 supervised 和daemon 有什么区别

* 在执行bgsave期间，执行的写命令如何保证数据一致的？